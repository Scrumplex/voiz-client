package net.scrumplex.voiz.client.net.packet

interface PacketIdentifier {

    /**
     * This function gets called when NetworkClient is looking for a PacketHandler to handle an incoming packet
     * @param data: full data of packet to identfy
     * @return true if packet was identified, false if packet is unidentified
     */
    fun identifyPacket(data: ByteArray): Boolean
}
