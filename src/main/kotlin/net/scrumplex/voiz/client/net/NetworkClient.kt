package net.scrumplex.voiz.client.net

import net.scrumplex.voiz.client.net.packet.PacketHandler
import net.scrumplex.voiz.client.audio.voice.VoicePacket
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.SocketAddress
import java.net.SocketTimeoutException
import java.nio.ByteBuffer

class NetworkClient(private val serverAddress: SocketAddress) : Thread("NetworkClient") {

    companion object {
        const val DEFAULT_PACKET_BUFFER = 1024
    }

    private val socket = DatagramSocket()
    private val packetHandlers = arrayListOf<PacketHandler>()

    init {
        socket.soTimeout = 1000
    }

    override fun run() {
        while (socket.isBound) {
            val data = ByteArray(DEFAULT_PACKET_BUFFER)
            val datagramPacket = DatagramPacket(data, data.size)

            try {
                socket.receive(datagramPacket)
            } catch (_: SocketTimeoutException) {
                continue
            }
            val handler = packetHandlers.find { it.identifyPacket(data) }
                ?: continue

            if (!handler.expired) {
                val packet = ServerPacket(datagramPacket)
                handler.handlePacket(packet)
            }

            if (handler.expired) // might have changed by now
                removePacketHandler(handler)
        }
    }

    fun send(buffer: ByteBuffer) {
        val datagramPacket = DatagramPacket(buffer.array(), buffer.remaining())
        datagramPacket.socketAddress = serverAddress
        socket.send(datagramPacket)
    }

    fun send(packet: VoicePacket) {
        send(packet.packetBuffer)
    }

    fun addPacketHandler(handler: PacketHandler) {
        handler.onActivated()
        packetHandlers.add(handler)
    }

    fun removePacketHandler(handler: PacketHandler) {
        packetHandlers.remove(handler)
    }

    override fun interrupt() {
        super.interrupt()
        socket.close()
    }
}
