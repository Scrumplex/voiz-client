package net.scrumplex.voiz.client.net.packet

import net.scrumplex.voiz.client.net.ProtocolDefaults

open class ControlPacketIdentifier(private val packetMode: ByteArray, private val minDataSize: Int = 8) :
    PacketIdentifier {

    private val packetHeader = ProtocolDefaults.CONTROLV0_HEADER // VZC.

    override fun identifyPacket(data: ByteArray): Boolean {
        return data.size >= packetHeader.size + packetMode.size + minDataSize
                && data.sliceArray(0 until 4).contentEquals(packetHeader)
                && data.sliceArray(4 until 8).contentEquals(packetMode)
    }
}
