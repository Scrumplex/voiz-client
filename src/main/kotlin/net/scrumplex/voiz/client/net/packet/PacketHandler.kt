package net.scrumplex.voiz.client.net.packet

import net.scrumplex.voiz.client.net.ServerPacket

interface PacketHandler : PacketIdentifier {

    /**
     * Defines if packet handler should be discarded after a packet was handled successfully.
     */
    var expired: Boolean

    /**
     * Gets called when PacketHandler enters packet handling queue
     * This is optional
     */
    fun onActivated() {

    }

    /**
     * This function gets called when this handler identified a packet and should now process it
     * @param serverPacket: packet to handle
     */
    fun handlePacket(serverPacket: ServerPacket)
}
