package net.scrumplex.voiz.client.net

import java.util.concurrent.CancellationException

class TimeoutException : CancellationException("The response timed out.")
