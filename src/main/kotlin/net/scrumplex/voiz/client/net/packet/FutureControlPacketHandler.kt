package net.scrumplex.voiz.client.net.packet

import net.scrumplex.voiz.client.net.ServerPacket
import java.util.concurrent.*

class FutureControlPacketHandler(
    packetMode: ByteArray,
    minDataSize: Int = 8,
    timeout: Long = 5000,
    timeUnit: TimeUnit = TimeUnit.MILLISECONDS
) :
    ControlPacketHandler(packetMode, minDataSize), Future<ServerPacket> {

    var state = State.PENDING
        private set

    override var expired = false

    private lateinit var packet: ServerPacket

    private val latch = CountDownLatch(1)

    val isTimedOut
        get() = state == State.TIMED_OUT || !::packet.isInitialized
    val isPending
        get() = state == State.PENDING || !::packet.isInitialized

    override fun isDone(): Boolean {
        return state == State.DONE
    }

    override fun isCancelled(): Boolean {
        return state == State.CANCELLED
    }

    private val timeoutThread = Thread {
        if (!isDone) {
            latch.await(timeout, timeUnit)
        }
        println("timoeut?")
        if (isPending) {
            println("timoeut!")
            state = State.TIMED_OUT
            latch.countDown()
            synchronized(expired) {
                expired = true
            }
        }
    }

    override fun onActivated() {
        timeoutThread.start()
    }

    override fun handlePacket(serverPacket: ServerPacket) {
        this.packet = serverPacket
        this.state = State.DONE
        latch.countDown()
        synchronized(expired) {
            expired = true
        }
    }

    override fun get(): ServerPacket {
        if (isPending) {
            latch.await()
        }
        if (isCancelled)
            throw CancellationException()
        if (isTimedOut)
            throw TimeoutException()
        return packet
    }

    override fun get(timeout: Long, unit: TimeUnit): ServerPacket {
        if (isPending) {
            latch.await(timeout, unit)
        }
        if (isCancelled)
            throw CancellationException()
        if (isTimedOut || !::packet.isInitialized)
            throw TimeoutException()
        return packet
    }

    override fun cancel(mayInterruptIfRunning: Boolean): Boolean {
        if (isPending) {
            state = State.CANCELLED
            latch.countDown()
            synchronized(expired) {
                expired = true
            }
            return true
        }
        return false
    }

    /**
     * Waits if necessary for the computation to complete, and then retrieves its result. If it did not succeed it returns null.
     * @return either packet if it already arrived or null
     */
    fun getOrNull(): ServerPacket? {
        return try {
            get()
        } catch (e: CancellationException) {
            return null
        } catch (e: TimeoutException) {
            return null
        }
    }

    enum class State {
        PENDING,
        DONE,
        CANCELLED,
        TIMED_OUT
    }

}

