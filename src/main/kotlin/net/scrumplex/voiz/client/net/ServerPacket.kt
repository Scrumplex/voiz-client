package net.scrumplex.voiz.client.net

import java.net.DatagramPacket
import java.nio.ByteBuffer

class ServerPacket(val datagramPacket: DatagramPacket) {
    val buffer: ByteBuffer = ByteBuffer.wrap(datagramPacket.data)

    init {
        buffer.position(datagramPacket.length)
        buffer.flip()
    }

}
