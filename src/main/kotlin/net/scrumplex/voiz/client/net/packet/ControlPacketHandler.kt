package net.scrumplex.voiz.client.net.packet

abstract class ControlPacketHandler(packetMode: ByteArray, minDataSize: Int = 8) : PacketHandler,
    ControlPacketIdentifier(packetMode, minDataSize)
