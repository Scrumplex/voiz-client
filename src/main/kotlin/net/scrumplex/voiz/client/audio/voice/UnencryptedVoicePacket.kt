package net.scrumplex.voiz.client.audio.voice

import net.scrumplex.voiz.client.net.ProtocolDefaults
import net.scrumplex.voiz.client.net.ServerPacket
import java.lang.RuntimeException
import java.nio.ByteBuffer

/**
 * Implementation of VoiZ voice packet type 0 (= generic unencrypted opus packet)
 * Explanation of packet header:
 *   <------------------- 8 Bytes -------------------->
 *   <------- 4 Bytes ------> <- 2 Bytes > <- 2 Bytes >
 *  |--------------------------------------------------|
 *  |      Magic header      |  clientId  | (reserved) |
 *  |--------------------------------------------------|
 *  |                                                  |
 *  |                      Data                        |
 *  |                                                  |
 *  |--------------------------------------------------|
 */
class UnencryptedVoicePacket : VoicePacket {
    override val packetBuffer: ByteBuffer
    override val voiceBuffer: ByteBuffer

    override val clientId: Short

    override val size: Int

    /**
     * Constructs VoicePacket object out of a DatagramPacket.
     * @throws RuntimeException When the packet header is not valid.
     */
    constructor(serverPacket: ServerPacket) {
        this.packetBuffer = serverPacket.buffer
        size = this.packetBuffer.remaining()

        if (!checkHeader())
            throw RuntimeException("Invalid packet received from ${serverPacket.datagramPacket.socketAddress}.")
        // position = 4
        this.clientId = packetBuffer.short

        packetBuffer.short  // skip 2

        // position = 8
        this.voiceBuffer = packetBuffer.slice()
    }

    constructor(clientId: Short, voiceBuffer: ByteBuffer) {
        this.packetBuffer = ByteBuffer.allocate(ProtocolDefaults.VOICEV0_BUFFER)

        // Add header to packet
        this.packetBuffer.put(ProtocolDefaults.VOICEV0_HEADER)

        this.clientId = clientId
        this.packetBuffer.putShort(clientId)

        this.packetBuffer.putShort(0) // add 2 bytes as buffer

        this.voiceBuffer = voiceBuffer
        this.packetBuffer.put(voiceBuffer)

        this.packetBuffer.flip()
        this.size = this.packetBuffer.remaining()
    }

    /**
     * Validates header of the data packet. After this method the packetBuffer's position is 4.
     * @return Returns true if packet starts with the magic header, false otherwise.
     */
    private fun checkHeader(): Boolean {
        packetBuffer.position(0)

        val header = byteArrayOf(packetBuffer.get(), packetBuffer.get(), packetBuffer.get(), packetBuffer.get())

        return header.contentEquals(ProtocolDefaults.VOICEV0_HEADER)
    }

}
