package net.scrumplex.voiz.client.audio

import net.scrumplex.voiz.client.audio.voice.VoicePacket
import net.scrumplex.voiz.client.pcmToAudio
import net.scrumplex.voiz.client.remainingArray
import tomp2p.opuswrapper.Opus
import java.nio.IntBuffer
import java.nio.ShortBuffer
import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioSystem

class AudioChannel(val clientId: Short, sampleRate: Int) {


    private val format = AudioFormat(sampleRate.toFloat(), 16, 1, true, true)
    private val audioOutput = AudioSystem.getSourceDataLine(format)
    private val encError = IntBuffer.allocate(4)
    private val pointer = Opus.INSTANCE.opus_decoder_create(sampleRate, 1, encError)

    init {
        audioOutput.open(format)
        audioOutput.start()
    }

    private fun decode(opusData: ByteArray, length: Int = opusData.size, pcmData: ShortBuffer, samples: Int): Int {
        val decoded = Opus.INSTANCE.opus_decode(
            pointer, opusData, length,
            pcmData, samples, 0
        )
        pcmData.position(decoded)
        pcmData.flip()
        return decoded
    }

    fun play(voicePacket: VoicePacket) {
        // OPUS
        val transferredBytes = voicePacket.voiceBuffer.remainingArray()
        val pcm = ShortBuffer.allocate(VoiceIO.AUDIO_BUFFER_SIZE)

        decode(transferredBytes, transferredBytes.size, pcm, VoiceIO.FRAME_SIZE)

        // PLAYBACK
        val audio = pcmToAudio(pcm)
        audioOutput.write(audio, 0, audio.size)
    }

    fun close() {
        audioOutput.close()
    }
}
