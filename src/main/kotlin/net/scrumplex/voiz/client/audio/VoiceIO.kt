package net.scrumplex.voiz.client.audio

import net.scrumplex.voiz.client.*
import net.scrumplex.voiz.client.audio.voice.UnencryptedVoicePacket
import net.scrumplex.voiz.client.audio.voice.UnencryptedVoicePacketHandler
import net.scrumplex.voiz.client.audio.voice.VoicePacket
import net.scrumplex.voiz.client.net.ProtocolDefaults
import tomp2p.opuswrapper.Opus
import java.nio.ByteBuffer
import java.nio.IntBuffer
import java.util.concurrent.TimeUnit
import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioSystem

class VoiceIO(private val remoteServer: RemoteServer) {

    companion object {
        const val SAMPLE_RATE = 48000.0f

        const val SAMPLE_SIZE = 16 // Bits
        const val FRAME_LENGTH = 20 // milliseconds

        /**
         * FRAME_LENGTH ms at SAMPLE_RATE Hz, e.g. 20ms @ 48kHz: 20*48 = 960
         */
        const val FRAME_SIZE = FRAME_LENGTH * (SAMPLE_RATE.toInt() / 1000)

        /**
         * JavaAudio gives us PCM as bytes. At 16Bits we need double the FRAME_SIZE as "space" for the buffer
         */
        const val AUDIO_BUFFER_SIZE = FRAME_SIZE * (SAMPLE_SIZE / 8)
    }

    private val format = AudioFormat(SAMPLE_RATE, 16, 1, true, true)
    private val audioInput = AudioSystem.getTargetDataLine(format)

    private val encError = IntBuffer.allocate(4)
    private val opusEncoder =
        Opus.INSTANCE.opus_encoder_create(SAMPLE_RATE.toInt(), 1, Opus.OPUS_APPLICATION_VOIP, encError)

    private val audioChannels = mutableListOf<AudioChannel>()

    var calculatedAudioVolume: Double = 0.0

    init {
        remoteServer.networkClient.addPacketHandler(UnencryptedVoicePacketHandler(this))
    }

    fun handleVoice(voicePacket: VoicePacket) {
        var channel = audioChannels.find {
            it.clientId == voicePacket.clientId
        }

        if (channel == null) {
            println("Created new audio channel for client ${voicePacket.clientId}.")
            channel = AudioChannel(voicePacket.clientId, SAMPLE_RATE.toInt())
            audioChannels.add(channel)
        }

        channel.play(voicePacket)
    }

    fun capture() {
        audioInput.open(format)
        audioInput.start()

        while (audioInput.isOpen) {
            // RECORDING
            var rawAudioData = ByteArray(AUDIO_BUFFER_SIZE)
            audioInput.read(rawAudioData, 0, rawAudioData.size)

            calculatedAudioVolume = calculateVolume(rawAudioData)

            val pcm = audioToPCM(rawAudioData)


            // OPUS
            val voiceBuffer = ByteBuffer.allocate(ProtocolDefaults.VOICEV0_BUFFER)
            val read =
                Opus.INSTANCE.opus_encode(
                    opusEncoder,
                    pcm,
                    FRAME_SIZE,
                    voiceBuffer,
                    voiceBuffer.remaining()
                )
            voiceBuffer.position(read)
            voiceBuffer.flip()

            // NETWORK
            val packet = UnencryptedVoicePacket(remoteServer.clientId, voiceBuffer)
            remoteServer.networkClient.send(packet)
        }
        println("Audio input has been closed!")
    }

    fun scheduleIOStats(ui: SwingUI) {
        remoteServer.scheduledPool.scheduleAtFixedRate({
            ui.updateVolume(calculatedAudioVolume.toInt())
        }, 250, 250, TimeUnit.MILLISECONDS)
    }
}
