package net.scrumplex.voiz.client.audio.voice

import java.nio.ByteBuffer

interface VoicePacket {

    val packetBuffer: ByteBuffer

    /**
     * ByteBuffer of original (unencrypted) opus data.
     */
    val voiceBuffer: ByteBuffer
    val clientId: Short

    val size: Int
}
