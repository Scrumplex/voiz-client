package net.scrumplex.voiz.client.audio.voice

import net.scrumplex.voiz.client.audio.VoiceIO
import net.scrumplex.voiz.client.net.ProtocolDefaults
import net.scrumplex.voiz.client.net.ServerPacket
import net.scrumplex.voiz.client.net.packet.PacketHandler

class UnencryptedVoicePacketHandler(private val voiceIO: VoiceIO) : PacketHandler {

    override var expired = false

    private val packetHeader = ProtocolDefaults.VOICEV0_HEADER // VZV.

    override fun identifyPacket(data: ByteArray): Boolean {
        return data.size >= 6 // minimum size to contain header and clientId
                && data.sliceArray(0 until 4).contentEquals(packetHeader)
    }

    override fun handlePacket(serverPacket: ServerPacket) {
        val voicePacket = UnencryptedVoicePacket(serverPacket)
        voiceIO.handleVoice(voicePacket)
    }

}