package net.scrumplex.voiz.client

import java.nio.ByteBuffer
import java.nio.ShortBuffer
import java.nio.charset.Charset
import kotlin.math.min
import kotlin.math.sqrt

fun ByteBuffer.remainingArray(): ByteArray {
    val array = ByteArray(remaining())
    get(array)
    return array
}

fun ByteBuffer.getString(maxLength: Int, charset: Charset = Charsets.UTF_8): String {
    val stringBytes = ByteArray(maxLength)

    for (i in 0 until min(remaining(), maxLength)) {
        stringBytes[i] = get()
        if (stringBytes[i] == 0x00.toByte())
            return String(stringBytes, 0, i, charset)
    }
    return String(stringBytes, charset)
}

fun String.toBufferedByteArray(target: ByteArray, charset: Charset = Charsets.UTF_8) {
    System.arraycopy(toByteArray(charset), 0, target, 0, min(target.size, length))
}

fun String.toBufferedByteArray(bufferSize: Int, charset: Charset = Charsets.UTF_8): ByteArray {
    val target = ByteArray(bufferSize)
    toBufferedByteArray(target, charset)
    return target
}

fun audioToPCM(rawAudioData: ByteArray): ShortBuffer {
    val pcm = ShortBuffer.allocate(rawAudioData.size / 2)
    for (i in 0 until rawAudioData.size / 2) {
        val b1 = rawAudioData[i * 2 + 1].toInt() and 0xff
        val b2 = rawAudioData[i * 2].toInt() shl 8
        pcm.put((b1 or b2).toShort())
    }
    pcm.flip()
    return pcm
}

fun pcmToAudio(pcm: ShortBuffer): ByteArray {
    val buffer = ByteArray(pcm.remaining() * 2)
    for (i in 0 until pcm.remaining()) {
        buffer[i * 2 + 1] = pcm[i].toByte()
        buffer[i * 2] = (pcm[i].toInt() shr 8).toByte()
    }
    return buffer
}

fun calculateVolume(data: ByteArray): Double {
    val average = data.average()

    var sumMeanSquare = 0.0

    for (value in data) {
        val f = value - average
        sumMeanSquare += f * f
    }
    val averageMeanSquare = sumMeanSquare / data.size

    return sqrt(averageMeanSquare)
}
