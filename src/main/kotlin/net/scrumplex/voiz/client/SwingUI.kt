package net.scrumplex.voiz.client

import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.FlowLayout
import javax.swing.*

class SwingUI : JFrame("VoiZ Client") {

    private val container = JPanel()
    private var layout = FlowLayout()

    private val volumeBar = JProgressBar()

    init {
        layout.alignment = FlowLayout.CENTER
        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE

        container.layout = layout

        container.add(volumeBar)

        contentPane.add(container, BorderLayout.CENTER)

        size = Dimension(300, 100)

        isVisible = true
    }

    fun updateVolume(volume: Int) {
        volumeBar.value = volume
    }

}
