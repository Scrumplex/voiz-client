package net.scrumplex.voiz.client

import net.scrumplex.voiz.client.audio.VoiceIO
import net.scrumplex.voiz.client.net.NetworkClient
import net.scrumplex.voiz.client.net.ProtocolDefaults
import net.scrumplex.voiz.client.net.ServerPacket
import net.scrumplex.voiz.client.net.packet.ControlPacketHandler
import net.scrumplex.voiz.client.net.packet.FutureControlPacketHandler
import java.net.SocketAddress
import java.nio.ByteBuffer
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import kotlin.RuntimeException

class RemoteServer(serverAddress: SocketAddress, val username: String) {

    val threadPool: ExecutorService = Executors.newFixedThreadPool(4)
    val scheduledPool: ScheduledExecutorService = Executors.newScheduledThreadPool(4)

    val networkClient = NetworkClient(serverAddress)

    val voiceIO = VoiceIO(this)

    var state = State.UNKNOWN
        private set
    var lastServerHeartbeat: Long = 0

    var serverName = ""
    var clientId: Short = 0

    init {
        threadPool.submit(networkClient)
        networkClient.addPacketHandler(HeartbeatPacketHandler(this))
    }

    fun hello(): Boolean {
        val helloBuffer = prepareBuffer(0x00, 0x00, dataSize = 0)
        helloBuffer.flip()

        val packetHandler =
            FutureControlPacketHandler(byteArrayOf(0x53, 0x43, 0x00, 0x00), 3)
        networkClient.addPacketHandler(packetHandler)

        networkClient.send(helloBuffer)

        val serverPacket = packetHandler.getOrNull()

        if (serverPacket == null) {
            state =
                if (packetHandler.isTimedOut) {
                    State.NOT_AVAILABLE
                } else {
                    State.UNKNOWN
                }
            return false
        }

        serverPacket.buffer.position(8) // skip header and mode

        state = State.AVAILABLE
        serverName = serverPacket.buffer.getString(32, ProtocolDefaults.CONTROLV0_CHARSET)

        return true
    }

    fun handshake(force: Boolean = false): Boolean {
        if (state == State.UP) {
            throw HandshakeException("Already up.")
        } else if (state == State.DOWN) {
            throw HandshakeException("Can not handshake after the connection has gone down.")
        } else if (state == State.NOT_AVAILABLE && !force) {
            throw HandshakeException("No server available. Pass the force flag to try anyway.")
        } else if (state == State.DROPPED) {
            TODO("Handle reconnection on connection loss")
        }

        val usernameBytes = username.toBufferedByteArray(32)

        val handshakeBuffer = prepareBuffer(0x00, 0x01)
        // Insert client username
        handshakeBuffer.put(usernameBytes)
        handshakeBuffer.flip()

        val packetHandler =
            FutureControlPacketHandler(byteArrayOf(0x53, 0x43, 0x00, 0x01), 2)
        networkClient.addPacketHandler(packetHandler)

        networkClient.send(handshakeBuffer)


        val serverPacket = packetHandler.getOrNull()

        if (serverPacket == null) {
            state =
                if (packetHandler.isTimedOut) {
                    State.NOT_AVAILABLE
                } else {
                    State.UNKNOWN
                }
            return false
        }

        serverPacket.buffer.position(8) // skip header and mode
        val proposedId = serverPacket.buffer.short

        return if (proposedId < 0) {
            state = State.ERROR_SERVER_FULL
            false
        } else {
            state = State.UP

            lastServerHeartbeat = System.currentTimeMillis()
            clientId = proposedId
            println("I AM $proposedId")
            true
        }
    }

    fun scheduleHeartbeatTimer() {
        scheduledPool.scheduleAtFixedRate({
            if (state == State.UP)
                heartbeat()
        }, 5000, 5000, TimeUnit.MILLISECONDS)
    }

    fun disconnect() {
        networkClient.interrupt()
        scheduledPool.shutdownNow()
        threadPool.shutdownNow()

        if (state == State.UP)
            state = State.DOWN
    }

    private fun heartbeat() {
        if (System.currentTimeMillis() - lastServerHeartbeat < ProtocolDefaults.CONTROLV0_TIMEOUT) {
            val heartbeatBuffer = prepareBuffer(0x00, 0x02, dataSize = 2) // heartbeat only needs to send a short
            heartbeatBuffer.putShort(clientId)
            heartbeatBuffer.flip()

            networkClient.send(heartbeatBuffer)
        } else {
            state = State.DROPPED
        }
    }

    private fun prepareBuffer(a1: Byte, a2: Byte, dataSize: Int = 248): ByteBuffer {
        val buffer = ByteBuffer.allocate(8 + dataSize)
        buffer.put(ProtocolDefaults.CONTROLV0_HEADER)
        // Set packet mode to "Client2Server"
        buffer.put(0x43)
        buffer.put(0x53)
        buffer.put(a1)
        buffer.put(a2)
        return buffer
    }

    enum class State {
        UNKNOWN,
        AVAILABLE,
        NOT_AVAILABLE,
        UP,
        DOWN,
        DROPPED,
        ERROR_SERVER_FULL,
        ERROR_PERMISSION_DENIED,
        ERROR_UNKNOWN
    }

    class HeartbeatPacketHandler(private val remoteServer: RemoteServer) :
        ControlPacketHandler(byteArrayOf(0x53, 0x43, 0x00, 0x02)) {
        override var expired = false

        override fun handlePacket(serverPacket: ServerPacket) {
            remoteServer.lastServerHeartbeat = System.currentTimeMillis()
        }
    }


    class HandshakeException(detailedMessage: String) :
        RuntimeException("Could not connect to the server: $detailedMessage")

}