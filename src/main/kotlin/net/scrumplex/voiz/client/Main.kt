package net.scrumplex.voiz.client

import club.minnced.opus.util.OpusLibrary
import net.scrumplex.voiz.client.net.ProtocolDefaults
import java.lang.Exception
import java.net.InetSocketAddress

fun main(args: Array<String>) {
    OpusLibrary.loadFromJar()

    if (args.isEmpty()) {
        error("Please specify the address you want to connect to as a parameter.")
    }

    val remoteServer = RemoteServer(InetSocketAddress(args[0], ProtocolDefaults.DEFAULT_PORT), "Scrumplex")

    if (remoteServer.hello()) {
        if (remoteServer.handshake()) {
            val ui = SwingUI()
            remoteServer.voiceIO.scheduleIOStats(ui)
            remoteServer.scheduleHeartbeatTimer()

            try {
                remoteServer.voiceIO.capture()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            println("Server did not respond.")
        }
    } else {
        println("Server does not exist.")
    }
    remoteServer.disconnect()
}
